package ru.kombarov.tm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        ProjectManager project = new ProjectManager();
        TaskManager task = new TaskManager();

        String[] helplist = {"help: Show all commands.", "project-clear: Remove all projects.", "project-create: Create new project.", "project-list: Show all projects.", "project-remove: Remove selected project.", "task-clear: Remove all tasks.", "task-create: Create new task.", "task-list: Show all tasks.", "task-remove: Remove selected task."};

        while (true) {
            String input = br.readLine();
            if (input.equals("help")) {
                for (String string:helplist) System.out.println(string);
            }

            if (input.equals("project-clear")) {
                project.projectClear();
            }

            if (input.equals("project-create")) {
                System.out.println("ENTER NAME");
                project.projectCreate(br.readLine());
            }

            if (input.equals("project-list")) {
                System.out.println("[PROJECT LIST]");
                project.projectList();
            }

            if (input.equals("project-list") && project.projectlist.isEmpty()) {
                System.out.println("PROJECT LIST IS EMPTY");
            }

            if (input.equals("project-remove")) {
                System.out.println("ENTER NAME");
                project.projectRemove(br.readLine());
            }

            if (input.equals("task-clear")) {
                task.taskClear();
            }

            if (input.equals("task-create")) {
                System.out.println("ENTER NAME");
                task.taskCreate(br.readLine());
            }

            if (input.equals("task-list")) {
                System.out.println("[TASK LIST]");
                task.taskList();
            }

            if (input.equals("task-list") && task.tasklist.isEmpty()) {
                System.out.println("TASK LIST IS EMPTY");
            }

            if (input.equals("task-remove")) {
                System.out.println("ENTER NAME");
                task.taskRemove(br.readLine());
            }
        }
    }
}
